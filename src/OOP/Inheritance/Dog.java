package OOP.Inheritance;

public class Dog extends Animal {       // This will allow us to use the animal class's constructor.


    private int eyes;
    private int legs;
    private int tail;
    private int teeth;
    private String coat;

    public Dog(String name, int size, int weight, int eyes, int legs, int tail, int teeth, String coat) {  //using the generator
        super(name, 1, 1, size, weight);          //super, calls the constructor from the extend class. in which we can also modify the dog.
        this.eyes = eyes;           //need to intialise all the new fields.
        this.legs = legs;
        this.tail = tail;
        this.teeth = teeth;
        this.coat = coat;
    }

    private void chew(){
        System.out.println( "Dog.chew(called)"
        );
    }

    @Override    // we are overriding a method in the super Class( eat() in Animal.class)
    public void eat() {
        System.out.println("Dog.eat() called");
        chew();
        super.eat(); // call the super equivalent of that class, which is animal. Therefore it will call this eat method first, then chew, then the super method.
    }

    public void walk(){
        System.out.println("Dog.walk() called");
        super.move(5);  // By using the super key word, will tell java not to execute a move method in the current class, but rather from teh super class.
    }

    public void run(){
        System.out.println("Dog.run() called");
        move(10);

    }

    private void moveLegs(int speed){
        System.out.println("Dog.moveLegs() called");
    }

    @Override
    public void move(int speed) {
        System.out.println("dog.move() called");
        moveLegs(speed);
        super.move(speed);
    }
}


/* Why calling move() is better than calling super.move(),
in the future we might want to make more unique characteristics

 */


/*
The eat and move methods from the animal class, will then be inherited/ available to the dog class because those methods are public.

* */