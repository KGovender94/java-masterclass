package OOP.Constructor;

public class Main {
    public static void main(String[] args) {

        Car porsche = new Car();
        Car holden = new Car();
        System.out.println(porsche.getModel());

        porsche.setModel("99");


        System.out.println("");
        System.out.println(porsche.getModel());
    }
}


/*
public is an access modifier, unrestricted.
private,  no other class has access.
Protected, only other classes in the same package can access it.

Classes are blueprints for objects.

encapsulation, internal workings of a objected can be only accessed from in
 */
