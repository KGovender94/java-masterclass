package OOP.ConstructorChallenge;

public class BankAccount {

    private String accountNumber;
    private double balance;
    private String fistName;
    private String email;
    String phoneNumber;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void deposit(double depositAmount) {
        this.balance += depositAmount;
        System.out.println("Deposit of " + depositAmount + "made. New balance is " + this.balance);
    }

    public void withdrawal(double withdrawalAmount) {
        if (this.balance - withdrawalAmount < 0) {
            System.out.println("You have fuckol money bru." + balance);
        } else {
            balance -= withdrawalAmount;
            System.out.println("Withdrawal of " + withdrawalAmount + "processed. Remaining balance = " + this.balance);
        }
    }

    public BankAccount() {
        this("56789", 2.50, "DefaultName", "DefaultEmail", "00000");
        System.out.println("Empty Constructor");
    }

    public BankAccount(String phoneNumber, double balance, String fistName, String email, String accountNumber) {
//        setPhoneNumber(phoneNumber);
        /* This way would allow us to use the validation from the setter method,
        this wont always work through, you should rather save the values from the constructor directly to the field as it guarantees the field
        values are initialised, it forces the value from the constructor.*/
        this.phoneNumber = phoneNumber;
        this.balance = balance;
        this.email = email;
        this.accountNumber = accountNumber;
        this.fistName = fistName;

        System.out.println("Account constructor with parameters called");
    }

    public BankAccount(String fistName, String email, String phoneNumber) {
        this(phoneNumber, 50, fistName, email, "99999"); // defaulted 2 parameters

    }
}
