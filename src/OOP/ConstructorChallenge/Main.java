package OOP.ConstructorChallenge;

public class Main {
    public static void main(String[] args) {
//        BankAccount bobsAccount = new BankAccount("0123456",0.00,"Bob","Bob@email.com","123456") ;//This will also run the constructor class in the BankAccount class.
        BankAccount bobsAccount = new BankAccount();//This will also run the constructor class in the BankAccount class.
        bobsAccount.withdrawal(100);

        bobsAccount.deposit(50);
        bobsAccount.withdrawal(100);
        bobsAccount.deposit(51);
        bobsAccount.withdrawal(100);

        BankAccount timsAccount = new BankAccount("tim","tim@email.com", "015245");
        System.out.println(timsAccount.getPhoneNumber() + " Tims number" + timsAccount.getBalance());

    }
}
