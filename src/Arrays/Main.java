package Arrays;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        double[] myDoubleArray = new double[10];
//        int[] myIntArray;
//        myIntArray = new int[10];
//        myIntArray[0] = 45; // This is assigning the value of 50 to the element 6
//        myIntArray[2] = 50;
//        myIntArray[5] = 50;

        //OR
//        int[] myIntArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // define all the values to the elements like this.
        //OR

        //using a for loop to initialize an array
        int[] myIntArray = new int[25];
        for (int i = 0; i < myIntArray.length; i++) {  //.length will look into the array for the size of the array.
            myIntArray[i] = i * 10;
        }
        //then printing out the values using a method
        printArray(myIntArray);


        System.out.println("------------------------------------------------------------------------");

        int[] myIntegers = getIntegers(5);
        for (int i = 0; i < myIntegers.length; i++) {
            System.out.println("Element " + i + ", typed value was " + myIntegers[i]);
        }
        System.out.println("The average is " + getAverage(myIntegers));
    }

    public static int[] getIntegers(int number) {
        System.out.println("Enter " + number + " integer values.\r");
        int[] values = new int[number];

        for (int i = 0; i < values.length; i++) {
            values[i] = scanner.nextInt();
        }

        return values;
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("Element " + i + ", value is " + array[i]);
        }
    }

    public static double getAverage(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return (double) sum / (double) array.length;
    }
}



/*
 * To create an array need to declare it with [] to tell compiler we are dealing with an arrray.
 * Cant delcare regular value into an array.
 * Array count starts at 0.
 * What you define the array as you must enter the same data type for the value.
 *
 * */