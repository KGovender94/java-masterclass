package DataTypesAndOperators;

public class ParsingValuesFromAString {
    public static void main(String[] args) {

        String numberAsString = "2018";
        System.out.println("numberAsString" + numberAsString);


        int number = Integer.parseInt(numberAsString);  //declared our variable number as int and parse it into string.
        System.out.println("number = " + number);

        numberAsString += 1;
        number += 1;

        System.out.println("numberAs String = " + numberAsString);
        System.out.println("number = " + number);
    }


}


/*
Integer is a wrapper class, which contains static classes such as parseInt to convert String into number type.
 */