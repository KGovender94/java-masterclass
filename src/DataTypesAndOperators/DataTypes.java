package DataTypesAndOperators;

public class DataTypes {
    public static void main(String[] args) {
        int myFirstNumber = 5 + 50; //declaring a variable
        int mySecondNumber = -12;
        int calculation = myFirstNumber + mySecondNumber;
        System.out.println(calculation);

        int myLastOne = 1000 - calculation;
        System.out.println(myLastOne);


        int myValue = 10000;

        int myMinIntValue = Integer.MIN_VALUE;
        int myMaxIntValue = Integer.MAX_VALUE;
        System.out.println("Min value : " + myMinIntValue);
        System.out.println("Max value : " + myMaxIntValue);
        System.out.println("Busted Max Value : " + (myMaxIntValue + 1));
        System.out.println("Busted Min Value : " + (myMinIntValue - 1));


        byte myMinByteValue = Byte.MIN_VALUE;
        byte myMaxByteValue = Byte.MAX_VALUE;
        System.out.println("Min value : " + myMinByteValue);
        System.out.println("Max value : " + myMaxByteValue);
        System.out.println("Busted Max Value : " + (myMaxByteValue + 1));
        System.out.println("Busted Min Value : " + (myMinByteValue - 1));


        short myMinShortValue = Short.MIN_VALUE;
        short myMaxShortValue = Short.MAX_VALUE;
        System.out.println("Min value : " + myMinShortValue);
        System.out.println("Max value : " + myMaxShortValue);
        System.out.println("Busted Max Value : " + (myMaxShortValue + 1));
        System.out.println("Busted Min Value : " + (myMinShortValue - 1));

        long myLongValue = 100L; //long is twice as long, need to include letter to tell java its a long value.
        long myMinLongValue = Long.MIN_VALUE;
        long myMaxLongValue = Long.MAX_VALUE;
        System.out.println("Min value : " + myMinLongValue);
        System.out.println("Max value : " + myMaxLongValue);
        System.out.println("Busted Max Value : " + (myMaxLongValue + 1));
        System.out.println("Busted Min Value : " + (myMinLongValue - 1));

//        Casting example , in the below, if you divide the minByte value by 2 you get an integer value, therefore we need to convert that value into a byte.
        byte myNewByteValue = (byte) (myMinByteValue / 2);

        short myNewShortValue = (short) (myMinShortValue / 2);


        byte byteValue = 10;
        short shortValue = 20;
        int intValue = 50;

        long longTotal = 5000L + 10L * (byteValue + shortValue + intValue);
        System.out.println(longTotal);

        short shortTotal = (short) (1000 + 10 *(byteValue + shortValue + intValue));
        System.out.println(shortTotal);


//        Float and Double
        float myMinFloatValue = Float.MIN_VALUE;
        float myMaxFloatValue = Float.MAX_VALUE;
        System.out.println("Min value : " + myMinFloatValue);
        System.out.println("Max value : " + myMaxFloatValue);
        System.out.println("Busted Max Value : " + (myMaxFloatValue + 1));
        System.out.println("Busted Min Value : " + (myMinFloatValue - 1));

        double myMinDoubleValue = Double.MIN_VALUE;
        double myMaxDoubleValue = Double.MAX_VALUE;
        System.out.println("Min value : " + myMinDoubleValue);
        System.out.println("Max value : " + myMaxDoubleValue);
        System.out.println("Busted Max Value : " + (myMaxDoubleValue + 1));
        System.out.println("Busted Min Value : " + (myMinDoubleValue - 1));


        int myIntValue = 5;
        float myFloatValue = 5.25f;
        double myDoubleValue = 5.25d;


//        Char and Boolean Characters
        char myChar  = 'D'; //Can only store one character, and use single quotes instead of double, used to story something last for instance in an array. occupies 2 bytes for unicode. check unicode-table.com
        char myUnicodeChar = '\u0044';
        System.out.println(myChar + " " + myUnicodeChar);

        boolean myTrueValue = true;
        boolean myFalseValue = false;
        boolean isCustomerOverTwentyOne = true;

//        String data type
        String myString = "this is a string";
        System.out.println(myString);
    }
}

/*
public is an access modifier
class , defines a class and curly braces define class body
void, wont return anything
varialbles  can be defined in the code, accessed by name, can be changed, has different data types.
primative Types, int, boolean, byte, char, long, short, float , double.
Casting, means converting one type or a number from one type to another
Floating number have fractional parts, with decimal points.(Float = single precision occupies 32bits, Double = double precision occupies 64bit).

String, sequence of characters, limited by max value of an int, they are immutable, cant change it after its created,
so java creates a new string that contains value of previous string, instead of modifying existing.

Operate
 */