package DataTypesAndOperators;

public class MethodOverloading {
    public static void main(String[] args) {

        int newScore = calScore("Kalin", 400);
        System.out.println("New score is " + newScore);
        calScore(75);
        calScore();
    }

    public static int calScore(String playerName, int score){
        System.out.println("player " + playerName + " scored " + score);
    return score * 1000;
    }

    public static int calScore(int score){
        System.out.println("Unnamed Player scored " + score);
        return score * 1000;
    }

    public static int calScore(){
        System.out.println("No player name, no player score");
        return 0;
    }
}


/*
Method Overloading is using the same method name with different parameters. what makes the method unique is the parameters, and not the name.

 */