package DataTypesAndOperators;

public class Methods {
    public static void main(String[] args) {

        calculateScore(true, 4000, 5, 100);

        calculateScore(true, 10000, 8, 200);
    }

    public static int calculateScore(boolean gameOver, int score, int levelCompleted, int bonus) {

        if (gameOver) { //(gameOver) basically equates to (gameOver == true)
            int finalScore = score + (levelCompleted * bonus);
            System.out.println("Your final score was " + finalScore);
    return finalScore;
        }

        return -1;
    }

}

/*
If you define parameters in the method, you dont have to create the variables in the method.
 */