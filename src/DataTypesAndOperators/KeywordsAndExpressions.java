package DataTypesAndOperators;

public class KeywordsAndExpressions {
    public static void main(String[] args) {

        //A mile is equal to 1.609344 kilometers
        double kilometers = (100 * 1.609344); //this is an expression except the data type of double and the semi-colon

        int highScore = 50;
        if (highScore == 50){ //only highScore == 50 is an expression.
            System.out.println("This is an expression"); //only what is inside the brackets is an expression.

        }

    }
}


/*
anything in blue is a reserved word

expressions are building blocks of java.
 */