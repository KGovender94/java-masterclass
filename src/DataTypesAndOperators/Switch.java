package DataTypesAndOperators;

public class Switch {
    public static void main(String[] args) {
        int value = 3;
        if (value == 1) {
            System.out.println("Value was 1");
        } else if (value == 2) {
            System.out.println("Value was 2");
        } else if (value == 3) {
            System.out.println("Value was not 1 or 2");
        }

        int switchValue = 3;
        switch (switchValue) {
            case 1:
                System.out.println("Value was 1");

                break; // terminates the switch statement

            case 2:
                System.out.println("Value was 2");

                break;

            case 3: case 4: case 5:
                System.out.println("was a 3, was a 4, was a 5");

                break;

            default: //default handles any other case possible if not stated above.
                System.out.println("Value was not 1 or 2, or 3,4,5");

                break;

        }
    }
}

/* Difference between IF statement and SWITCH statement, both acheive the same thing, if statement
is more flexible, we dont have to use the same test criteria, in terms of the switch youre only testing
the switchValue
 */