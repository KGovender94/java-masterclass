package DataTypesAndOperators;

public class Operaters {

    public static void main(String[] args) {

        int AddResult = 1 + 2;
        System.out.println(AddResult);
        int minusResult = 1 - 2;
        System.out.println(minusResult);

        int MultipleResult = 1 * 2;
        System.out.println(MultipleResult);

        int DivideResult = 1 / 2;
        System.out.println(DivideResult);

        int percentageResult = 4 % 3;
        System.out.println(percentageResult);


//        abbreviating operators
        AddResult++; //Last result + 1
        System.out.println("3 + 1 = " + AddResult);

        AddResult--; // Last result - 1
        System.out.println("4 + 1 = " + AddResult);

        AddResult += 2; // Last result + another number
        System.out.println("3 + 2 = " + AddResult);

        AddResult *= 2; // Last result times another number
        System.out.println("5 * 2 = " + AddResult);

        AddResult /= 2; // Last result times another number
        System.out.println("10 * 2 = " + AddResult);

//        if-then statements
        boolean isAlien = false; // = assignment operator , alien is set to false
        if (isAlien == false) {
            // == sees if operands are identical, in which the variable isAlien is set as false, and check if it is false, therefore this statement is true. if isAlien == true, code block wont run.
            System.out.println("Isnt an Alien");
            System.out.println("scared of them");
        }

        int topScore = 80;
        if (topScore >= 100) { // can use <= , == , >
            System.out.println("high score!");
        }

        int secondTopScore = 60;
        if ((topScore > secondTopScore) && (topScore < 100)) {  //This is the and operator && needs both operands to be met
            System.out.println("Greater than second top score and less than 100");
        }

        if ((topScore > 90) || (secondTopScore <= 90)) {  //This is the OR operator ||

            System.out.println("either or both conditions are true");

        }

        int newValue = 50;
        if (newValue == 50) {
            /*
         (newValue = 50) would produce an error, cause we are using the assignment operator( 1 = sign) to assign the value. to fix this we need to use the equal to operator( == ).
         doesn't assign value but tests to see if operands equal.*/
            System.out.println("This is true");
        }

        boolean isCar = false;
        if (isCar) {    //This wont produce an error because we set the value of isCar as boolean, and using the assignment operator is looking for a boolean operand.
            System.out.println("This is not suppose to happen");
        }

        isCar = true;
        boolean wasCar = (isCar) ? true : false;  /*This has 3 operands, therefore refers to the ternary operands.
        first operand isCar is the condition were testing, true is the value were assigning to wasCar if the first condition is true, and the third operand is what is assigned
        to wasCar if first condition is false.
    */
        if (wasCar){   //wasCar = true
            System.out.println("wasCar is true");
        }


//        operator challenge
        double myFirstValue = 20.00d;
        double mySecondValue = 80.00d;
        double myValueTotal = (myFirstValue + mySecondValue) * 100.00d;
        System.out.println("MyValueTotal = " + myValueTotal);
        double theRemainder  = myValueTotal % 40.00d;
        System.out.println("theRemainder = " + theRemainder);
        boolean isNoRemainder = (theRemainder == 0) ? true : false;
        System.out.println("isNoRemainder = " + isNoRemainder);
        if (!isNoRemainder){
            System.out.println("Got some Remainder");
        }

    }
}

/*
operator ,  + - * /
operand, any object that is manipulated by operator, eg the variables in an expression.
if then rule - always use a code block.

Logical AND &&, operates on boolean operands, checks if given condition is true or false
Logical OR  ||, operates on boolean operands, checks if given condition is true or false
The | is a bitwise operator, working at a bit level.

The NOT operator,
(isCar) tests for true
(!isCar) tests for false

Under the difference between assignment operator  =  and equal to operator  ==
1. for isCar example, isCar was set to false,and using the assignment operator to set it true would still run the block, but if we use the equal to operator, it wont run the block.
 */