package DataTypesAndOperators;

import Excerises.*;

public class SpeedConverterTestClass {

    public static void main(String[] args) {

        long miles = SpeedConverter.toMilesPerHour(60); //Basically save the outcome in a variable(miles), and printed it out.
        System.out.println("miles = " + miles);

        SpeedConverter.printConversion(60);


        MegaByteConverter.printMegaByteAndKiloBytes(2500);

        BarkingoDog.bark(true,7);

        LeapYearCalculator.isLeapYear(1700);

        DecimalComparator.areEqualByThreeDecimalPlaces(-3.1034,-3.1035);

        EqualSumChecker.hasEqualSum(2,-1,0);

        TeenNumberChecker.hasTeen(1,18,15);
    }
}
