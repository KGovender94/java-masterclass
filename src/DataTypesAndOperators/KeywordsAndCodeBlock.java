package DataTypesAndOperators;

public class KeywordsAndCodeBlock {
    public static void main(String[] args) {
        boolean gameOver = true;
        int score = 4000;
        int levelCompleted = 5;
        int bonus = 100;

        if (score < 5000 && score > 2000) {
            System.out.println("Your score was " + score + " less than 5000, but greater than 1000");
        } else if (score < 1000) {
            System.out.println("Your score was less than 1000");
        } else {
            System.out.println("Got Here");
        }

        if (gameOver) { //(gameOver) basically equates to (gameOver == true)
            int finalScore = score + (levelCompleted * bonus);
            System.out.println("Your final score was " + finalScore);
        }

        if (gameOver == true) {
            score = 10000;
            levelCompleted = 8;
            bonus = 200;

            int finalScore = score + (levelCompleted * bonus);
            System.out.println("Your final score was " + finalScore);
        }
    }

}


/*
CodeBlocks are able to access global variables outside the block and variables created inside the code block, but variables created inside the codeblock cant be accessed from outside.
 */