package DataTypesAndOperators;

public class ForStatement {

    public static void main(String[] args) {

        System.out.println("10000 at 2% interest =" + calculateInterest(10000.0, 2.0));
        System.out.println("10000 at 2% interest =" + calculateInterest(10000.0, 3.0));
        System.out.println("10000 at 2% interest =" + calculateInterest(10000.0, 4.0));
        System.out.println("10000 at 2% interest =" + calculateInterest(10000.0, 5.0));


        for (int i = 0; i < 5; i++) { // this we set i to 0, we going to test that i is less than 5, and i++ means we are adding 1 each time to i.
            System.out.println("loop " + i + " hello");
        }

        for (double i = 2; i < 9; i++) {
            System.out.println("10000 at " + i + "% interest =" + calculateInterest(10000.0, i));
//            System.out.println("10000 at " + i + "% interest =" + String.format("%.2f", calculateInterest(10000.0, i)));
        }

        System.out.println("*********************");

        for (double i = 8; i > 1; i--) {
            System.out.println("10000 at " + i + "% interest =" + calculateInterest(10000.0, i));
        }

        int count = 0;
        for (int i=10; i<50; i++) {
            if (isPrime(i)) {
                count++;
                System.out.println("Number " + i + " is a prime number.");
                if (count == 3) {
                    System.out.println("Exiting loop");
                    break;
                }
            }
        }



    }

    public static double calculateInterest(double amount, double interestRate) {

        return (amount * (interestRate / 100));
    }

    public static boolean isPrime(int n) {
        if (n == 1) {
            return false;
        }

        for (int i = 2; i <= n / 2; i++) {
            if ((n % i) == 0) {
                return false;
            }
        }
        return true;
    }


}


/*
for used to loop code, execute code a certain number of times.
for(init; terminate; increment){}
loop variables, e.g i only exist in the loop code block.
init, code is going to be initialized  once at the start of the loop.
terminate, how we want to exit, evaluate the false.
increment, number of times
 */



/*
Challenge:
1using the for statement, call the calculateInterest method with,
the amount of 10000 with an interestRate of 2,3,4,5,6,7 and 8,
and print the result.

2.Modify the above loop to do the same but start from 8% to 2%

3. Create a for statement using any range of numbers, then determine if the number is a prime using isPrime method,
if it is a prime, print it and increment a count of the number of prime numbers found, if the count is 3 exit
* */
