package DataTypesAndOperators;

import java.util.Scanner;

public class ReadingUserInput {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter birth year :");
        boolean hasNextInt = scanner.hasNextInt();  //hasNextInt checks to see if the input is a number value.
        if(hasNextInt){

            int yearOfBirth = scanner.nextInt(); // nextInt automatically takes input and converts into integer.
            scanner.nextLine();

            System.out.println("Enter name : ");
            String name = scanner.nextLine();
            int age = 2020 - yearOfBirth;

            if (age >= 0 && age <=100){
                System.out.println("your name is :" + name + "and you are " + age );
            }

            else{
                System.out.println("Invalid year of Birth");
            }
        }
        else{
            System.out.println("Invalid value, please enter a year in numbers");
        }


        scanner.close();


    }
}
