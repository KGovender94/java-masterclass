package DataTypesAndOperators;

public class PostiveNegativeZero {
    public static void main(String[] args) {

        checkNumber(5);
    }

    public static void checkNumber(int number){
        if(number > 0){
            System.out.println("Number is positive");
        }
        else if (number < 0){
            System.out.println("Number is negative");
        }
        else if(number == 0){
            System.out.println("Number is zero");
        }
    }
}
