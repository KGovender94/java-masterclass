package DataTypesAndOperators;

public class StatementsWhiteSpacesAndIndenting {
    public static void main(String[] args) {

        int myVariable = 50; //the entire line is a statement.
        myVariable++; //this is also a statement.

    }
}
