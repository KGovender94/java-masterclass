package DataTypesAndOperators;

public class WhileAndDoWhileStatements {

    public static void main(String[] args) {
        int count = 6;
//        while(count != 6){
//            System.out.println("Count value is " + count);
//            count ++;
//        }
//
//        for (count = 1; count != 6; count++){
//            System.out.println("Count value is " + count);
//        }

//        count = 6;
//        do {
//            System.out.println("Count value was " + count);
//            count++;
//
//            if (count > 100){
//                break;
//            }
//        }
//        while(count != 6);

//        int num = 4;
//        int finishNumber = 20;
//
//        while (num <= finishNumber) {
//            num++;
//            if (!isEvenNumber(num)) {
//                continue;
//            }
//
//            System.out.println("Even number " + num);
//        }


        int num = 4;
        int finishNumber = 20;
        int evenNumFound = 0;

        while (num <= finishNumber) {
            num++;
            if (!isEvenNumber(num)) {
                continue;
            }

            System.out.println("Even number " + num);

            evenNumFound++;
            if (evenNumFound >= 5){
                break;
            }
        }


    }


    public static boolean isEvenNumber(int num) {
        if ((num % 2) == 0) {
            return true;
        } else {
            return false;
        }
    }
}


/*
While statement , can be looped until expression is true.


Challenge
1.  // Create a method called isEvenNumber that takes a parameter of type int
    // Its purpose is to determine if the argument passed to the method is
    // an even number or not.
    // return true if an even number, otherwise return false;

2.
    // Modify the while code above
    // Make it also record the total number of even numbers it has found
    // and break once 5 are found
    // and at the end, display the total number of even numbers found

 */