package LinkedLists;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Customer customer = new Customer("Kalin", 50);
        Customer anotherCustomer;
        anotherCustomer = customer;
        anotherCustomer.setBalance(40);
        System.out.println("Balance for customer " + customer.getName() + " is " + customer.getBalance());

        ArrayList<Integer> intList = new ArrayList<Integer>();

        intList.add(1);
        intList.add(3);
        intList.add(4);

        for (int i=0; i<intList.size(); i++){
            System.out.println(i + ": " + intList.get(i));
        }

        intList.add(1,2);

        for (int i=0; i<intList.size(); i++){
            System.out.println(i + ": " + intList.get(i));
        }


        //The above is simple but with a larger array will use up memory excessively.
        // linked list is an altervative to arrays, it stores the link to the next item in the list and the data.
        //Each element in the list actually holds a link to the item than follows it as well as the the value you want to store.


    }
}
