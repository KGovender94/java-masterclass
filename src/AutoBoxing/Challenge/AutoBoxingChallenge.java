package AutoBoxing.Challenge;

public class AutoBoxingChallenge {
    public static void main(String[] args){


        Bank bank = new Bank("National Bank");

        if (bank.addBranch("pta")) {
            System.out.println("branch pta created.");
        }
        bank.addCustomer("pta","Kalin",50.05);
        bank.addCustomer("pta","Kamy",75.05);
        bank.addCustomer("pta","Merlin",175.05);

        bank.addBranch("jhb");
        bank.addCustomer("jhb", "bob", 150);

        bank.addCustomerTransaction("pta","Kalin",50);
        bank.addCustomerTransaction("pta","Kalin",50);
        bank.addCustomerTransaction("pta","Kamy",50);

        bank.listCustomers("pta",false);
        System.out.println("----------------------------------------");
        bank.listCustomers("pta",true);
        System.out.println("----------------------------------------");
        bank.listCustomers("jhb",true);

        if (!bank.addCustomer("cpt","brian",50)){
            System.out.println("Error cpt doesnt exist.");
        }

        if (!bank.addBranch("pta")){
            System.out.println("branch already exists");

        }

        if (!bank.addCustomerTransaction("pta","fergus",50)){
            System.out.println("Customer doesnt exist at branch pta");
        }

    }
}
