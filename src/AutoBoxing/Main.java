package AutoBoxing;

import java.util.ArrayList;

class intClass {
    private int myValue;

    public intClass(int myValue) {
        this.myValue = myValue;
    }

    public int getMyValue() {
        return myValue;
    }

    public void setMyValue(int myValue) {
        this.myValue = myValue;
    }
}

public class Main {
    public static void main(String[] args) {
        String[] strArray = new String[10];
        int[] intArray = new int[10];


        ArrayList<String> strArrayList = new ArrayList<String>();
        strArrayList.add("Tim");


//        ArrayList<int> intArrayList  = new ArrayList<int>(); // Cant do this because we need a class reference to save something to an array list. see Example below
        ArrayList<intClass> intClassArrayList = new ArrayList<intClass>(); //this references the intClass above.
        intClassArrayList.add(new intClass(54));

        //OR

//        Integer integer = new Integer(54); // Integer is class, int is not therefore this will work.
//
//        ArrayList<Integer> intArrayList = new ArrayList<Integer>();
//        for (int i = 0 ; i<=10; i++){
//            intArrayList.add(Integer.valueOf(i)); // takes the value of i as the primative type and converting it to integer class ( this is an example of autoboxing -  take primative type  into an integer)
//
//        }
//
//        for (int i = 0 ; i<=10; i++) {
//            System.out.println(i + " -->" + intArrayList.get(i).intValue()); // this is an example of unboxing - taking from object wrapper and converting back to primative type.
//        }


        //there is an easier way, this is preferred.

        Integer myIntValue = 56; // java shortcut to set new integer value essentially this line is the same as Integer.valueOf(56);
        int myInt  =  myIntValue.intValue(); // this java shortcut essentially says myInt.intValue();



        ArrayList<Double> myDoubleValues = new ArrayList<Double>();
        for (double dbl=0.0; dbl<10.0; dbl += 0.5){
            myDoubleValues.add(dbl); // converts the primative type dbl to the object type Double wrapper.
        }

        for (int i=0; i<myDoubleValues.size(); i++){
            double value = myDoubleValues.get(1); //unboxes the object Double wrapper back to primative type.
            System.out.println(i + " --> " +value);
        }

        }
    }


// AutoBoxing will solve the above.

//Remember String is a class in its own right , just like Integer, Double.

