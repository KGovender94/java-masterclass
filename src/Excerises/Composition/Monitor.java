package Excerises.Composition;

public class Monitor {

    private String model;
    private String manufacture;
    private int size;
    private Resolution nativeRes;// this is an example of composition, the monitor isn't a resolution but has a resolution

    public Monitor(String model, String manufacture, int size, Resolution nativeRes) {
        this.model = model;
        this.manufacture = manufacture;
        this.size = size;
        this.nativeRes = nativeRes;
    }

    public void drawPixelAt(int x, int y, String colour) {
        System.out.println("Drawing pixel at " + x + " , " + y + " in colour " + colour);
    }

    public String getModel() {
        return model;
    }

    public String getManufacture() {
        return manufacture;
    }

    public int getSize() {
        return size;
    }

    public Resolution getNativeRes() {
        return nativeRes;
    }
}
