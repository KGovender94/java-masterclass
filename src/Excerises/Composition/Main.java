package Excerises.Composition;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        Dimensions dimension = new Dimensions(20, 20, 5);

        Case theCase = new Case("220", "Dell", "240", dimension);
        Monitor theMonitor = new Monitor("27inch Beast", "Acer", 27, new Resolution(2540, 1441));
        Motherboard motherboard = new Motherboard("200", "asus", 4, 6, "v2.44");

        PC thePC = new PC(theCase, theMonitor, motherboard);
        thePC.powerUp();


    }
}

