package Excerises;


public class OverloadingMethodChallenge {

    public static void main(String[] args) {
        double centimeters = calcFeetAndInchesToCentimeters(7, 13);
        if (centimeters < 0.0) {
            System.out.println("Invalid parameters");
        }

        calcFeetAndInchesToCentimeters(100);
    }

    public static double calcFeetAndInchesToCentimeters(double feet, double inches) {
        if ((feet < 0) || ((inches < 0) || (inches > 12))) {
            System.out.println("Invalid feet or inches parameters");
            return -1; // is a good way to test for validation, as -1 is commonly returned if there is something wrong the code.
        }

        double centimeters = (feet * 12) * 2.54;
        centimeters += inches * 2.54;
        System.out.println(feet + "feet, " + inches + " inches = " + centimeters + " cm");
        return centimeters;
    }

    public static double calcFeetAndInchesToCentimeters(double inches) {
        if (inches < 0) {
            return -1;
        }

        double feet = (int) inches / 12;
        double remainInches = (int) inches % 12;
        System.out.println(inches + " inches = " + feet + "feet and  " + remainInches + "inches");
        return calcFeetAndInchesToCentimeters(feet, remainInches);
    }
}

