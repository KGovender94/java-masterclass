package Excerises.Polymorphism.PolymorphismChallenge;

class Car {
    private boolean engine;
    private int cylinder;
    private String name;
    private int wheels;

    public Car(int cylinder, String name) {
        this.cylinder = cylinder;
        this.name = name;
        this.wheels = 4;
        this.engine = true;
    }


    public int getCylinder() {
        return cylinder;
    }

    public String getName() {
        return name;
    }

    public String startEngine() {
        return "Car -> start engine";
    }


    public String accelerate() {
        return "Car -> accelerate";
    }

    public String brake() {
        return "Car -> brake";
    }

}

class Mitshubishi extends Car {

    public Mitshubishi(int cylinder, String name) {
        super(cylinder, name);
    }

    @Override
    public String startEngine() {
        return "Mitshubishi -> start engine";
    }

    @Override
    public String accelerate() {
        return "Mitshubishi -> accelerate";
    }

    @Override
    public String brake() {
        return "Mitshubishi -> brake";
    }
}

public class Main {
    public static void main(String[] args) {

        Car car = new Car(8, "base Car");
        System.out.println(car.startEngine());
        System.out.println(car.accelerate());
        System.out.println(car.brake());

        Mitshubishi mitshubishi = new Mitshubishi(8, "outlander");
        System.out.println(mitshubishi.startEngine());
        System.out.println(mitshubishi.accelerate());
        System.out.println(mitshubishi.brake());

        Nissan nissan = new Nissan(6, "350z");
        System.out.println(nissan.startEngine());
        System.out.println(nissan.accelerate());
        System.out.println(nissan.brake());

        Ford ford = new Ford(4, "feista");
        System.out.println(ford.startEngine());
        System.out.println(ford.accelerate());
        System.out.println(ford.brake());


    }

    static class Ford extends Car {

        public Ford(int cylinder, String name) {
            super(cylinder, name);
        }

        @Override
        public String startEngine() {
            return getClass().getSimpleName() + " Ford -> start engine";
        }

        @Override
        public String accelerate() {
            return getClass().getSimpleName() + " Ford -> accelerate";
        }

        @Override
        public String brake() {
            return getClass().getSimpleName() + " Ford -> brake";
        }
    }
}


//getClass().getSimpleName()  is a simple way to get the class name.