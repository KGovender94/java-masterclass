package Excerises;

public class SwitchExercise {
    public static void main(String[] args) {

        char switchValue = 'A';

        switch (switchValue) {

            case 'A':
                System.out.println("Is an A");
                break;

            case 'B':
                System.out.println("Is an B");
                break;

            case 'C': case 'D': case 'E':
                System.out.println("Is an C, or D, or E");
                break;

            default:
                System.out.println("Couldnt find");
                break;
        }


    String month  = "janu";

        switch(month.toLowerCase()){

            case "jan":
                System.out.println("Jan");
                break;

        }
    }
}
