package Excerises;

import java.util.Scanner;

public class ReadingUserInput {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int counter = 0;
        int sum = 0;

//        while (true){           // creates an endless loop. alternatively we can set the counter limit in the while condition as below.
        while (counter < 10) {
            int order = counter + 1;
            System.out.println("Enter number # " + order + " :");

            boolean isAnInt = scanner.hasNextInt();   //checks to see if valid number has been entered.

            if (isAnInt) {
                int number = scanner.nextInt();
                counter++;
                sum += number;    //
                if (counter == 10) {
//                    break;
                }
            } else {
                System.out.println("Invalid number");
            }

            scanner.nextLine();  //Handle end of line enter key
        }
        System.out.println(" Sum = " + sum);
        scanner.close();
    }
}
