package Excerises.OOPChallenge;

public class Main {

    public static void main(String[] args) {

        Burger burger = new Burger("basic", "mutton", 3.56, "white");
        double price = burger.itemizeBurger();
        burger.addBurgerAddition1("Tomato", 0.27);
        burger.addBurgerAddition2("Cheese", 0.75);
        burger.addBurgerAddition3("lettuce", 1.13);
        System.out.println("Total burger price : " + burger.itemizeBurger());


        HealthBurger healthBurger = new HealthBurger("bacon", 5.67);
        healthBurger.addBurgerAddition1("egg", 5.43);
        healthBurger.addHealthAddition1("Lentils", 3.41);
        System.out.println("Total Healthy Burger price is " + healthBurger.itemizeBurger());

        DeluxeBurger deluxeBurger = new DeluxeBurger();
        deluxeBurger.addBurgerAddition3("macon", 50.43);
        deluxeBurger.itemizeBurger();
    }
}
