package Excerises.Ecapsulation;
//NO encapsulation

public class Main {

    public static void main(String[] args) {

//        Player player = new Player();
//        player.name = "Kalin";
//        player.health = 20;
//        player.weapon = "sword";
//
//        int damage = 10;
//        player.loseHeath(damage);
//        System.out.println("Remaining health : " + player.healthRemaining());
//
//        damage = 11;
//        player.loseHeath(damage);
//        System.out.println("Remaining health : " + player.healthRemaining());


        EnhancedPlayer player =  new EnhancedPlayer("Kalin",200,"Sword");
        System.out.println("Initial health is  " + player.getHealth());
    }
}



/*Encapsulation allows you to restrict access to certain components in objects.
* Will be able to protect members of the class from external access*/

// From the player class, we can access the fields directly because the scope is set to public.