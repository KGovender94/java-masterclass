package Excerises.Ecapsulation;

public class EnhancedPlayer {

    private String name;
    private int health = 100;
    private String weapon;

    public EnhancedPlayer(String name, int health, String weapon) {
        this.name = name;
        if (health > 0 && health <= 100) {
            this.health = health;  //This is basic validation, only values we accept for health.
        }
        this.weapon = weapon;
    }
//This guarantees the variables are initialised when the class is created.

    public void loseHeath(int damage) {
        this.health = this.health - damage;
        if (this.health <= 0) {
            System.out.println("PLayer knocked out");

        }
    }


    public int getHealth() {
        return health;
    }
}
