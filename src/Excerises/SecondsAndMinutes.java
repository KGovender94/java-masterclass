package Excerises;

public class SecondsAndMinutes {
    private static final  String INVALID_VALUE_MESSAGE = "Invalid Value";

    public static void main(String[] args) {

        System.out.println(getDurationString(65, 45));
        System.out.println(getDurationString(3945L));
        System.out.println(getDurationString(-41));
        System.out.println(getDurationString(65, 9));

    }

    public static String getDurationString(long minutes, long seconds) {
        if ((minutes < 0) || (seconds < 0) || (seconds > 59)) {
            return INVALID_VALUE_MESSAGE;
        }


        long hours = minutes / 60;
        long remaingMinutes = minutes % 60;

        String hourString = hours + "h";
        if (hours < 10) {
            hourString = "0" + hourString;
        }

        String minutesString = remaingMinutes + "m";
        if (remaingMinutes < 10) {
            minutesString = "0" + minutesString;
        }

        String secondString = seconds + "s";
        if (seconds < 10) {
            secondString = "0" + secondString;
        }

        return hourString + " " + minutesString + " " + secondString + "";
    }

    public static String getDurationString(long seconds) {
        if (seconds < 0) {
            return INVALID_VALUE_MESSAGE;
        }

        long minutes = seconds / 60;
        long remainSeconds = seconds % 60;

        return getDurationString(minutes, remainSeconds);
    }
}


/*constants cant be changed,
* cannot assign value to final variable.
* constants are usually typed in uppercase for indentification.
* */