package Excerises;

public class NumberInWord {
    public static void main(String[] args) {

        printNumberInWords("One");
    }

    public static void printNumberInWords(String number) {

        switch (number.toLowerCase()) {
            case "one":
                System.out.println(1);
                break;

            case "two":
                System.out.println(2);
                break;

            case "three":
                System.out.println(3);
                break;

            case "four":
                System.out.println(4);
                break;

            case "five":
                System.out.println(5);
                break;

            case "six":
                System.out.println(6);
                break;

            case "seven":
                System.out.println(7);
                break;

            case "eight":
                System.out.println(8);
                break;

            case "nine":
                System.out.println(9);
                break;

            default:
                System.out.println("other");
        }

    }

}


/*
Write a method called printNumberInWord. The method has one parameter number which is the whole number.
The method needs to print "ZERO", "ONE", "TWO", ... "NINE", "OTHER" if the int parameter number is 0, 1, 2, .... 9
or other for any other number including negative numbers.
You can use if-else statement or switch statement whatever is easier for you.

NOTE: Method printNumberInWord needs to be public static for now, we are only using static methods.
NOTE: Do not add main method to solution code.
 */