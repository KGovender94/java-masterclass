package ArrayList.Challenge;

import java.util.ArrayList;

public class MobilePhone {

    private String myNumber;
    private ArrayList<Contact> myContactList;

    public MobilePhone(String myNumber) {
        this.myNumber = myNumber;
        this.myContactList = new ArrayList<Contact>();
    }

    public boolean addNewContact(Contact contact) {
        if (findContact(contact.getName()) >= 0) {
            System.out.println("Contact is already on file");
            return false;
        }

        myContactList.add(contact);
        return true;
    }

    private int findContact(Contact contact) {
        return this.myContactList.indexOf(contact);
    }

    private int findContact(String name) {
        for (int i = 0; i < this.myContactList.size(); i++) {
            Contact contact = this.myContactList.get(i);

            if (contact.getName().equals(name)) {
                return 1;
            }
        }
        return -1; // if contact isnt found return -1/error
    }

    public String queryContact(Contact contact) {
        if (findContact(contact) >= 0) {
            return contact.getName();

        }
        return null;
    }

    public Contact queryContact(String name) {
        int position = findContact(name);
        if (position >= 0) {
            return this.myContactList.get(position);

        }
        return null;
    }

    public boolean updateContact(Contact oldContact, Contact newContact) {
        int foundPosition = findContact(oldContact);
        if (foundPosition < 0) {
            System.out.println(oldContact.getName() + "was not found.");
            return false;
        }
        else if (findContact(newContact.getName()) != -1){
            System.out.println("Contact with name" + newContact.getName() +
            "already exists. Update unsuccesful");
        }
        this.myContactList.set(foundPosition, newContact);
        System.out.println(oldContact.getName() + " was replaced with " + newContact.getName());
        return true;
    }

    public boolean removeContact(Contact contact) {
        int foundPosition = findContact(contact);
        if (foundPosition < 0) {
            System.out.println(contact.getName() + "was not found.");
            return false;
        }

        this.myContactList.remove(foundPosition);
        System.out.println(contact.getName() + " was removed.");
        return true;

    }

    public void printContacts() {
        System.out.println("Contact List");
        for (int i = 0; i < myContactList.size(); i++) {
            System.out.println((i + 1) + "." +
                    this.myContactList.get(i).getName() + " ->" + this.myContactList.get(i).getPhoneNumber());
        }
    }
}
