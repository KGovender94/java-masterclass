package ArrayList;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayLists {
    private static Scanner scanner = new Scanner(System.in);
    private static GroceryList groceryList = new GroceryList();

    public static void main(String[] args) {
        boolean quit = false;
        int choice = 0;
        printInstructions();
        while (!quit) {
            System.out.println("Enter your choice: ");
            choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 0:
                    printInstructions();
                    break;
                case 1:
                    groceryList.printGroceryList();
                    break;
                case 2:
                    addItem();
                    break;
                case 3:
                    modifyItem();
                    break;
                case 4:
                    removeItem();
                    break;
                case 5:
                    searchItem();
                    break;
                case 6:
                    quit = true;
                    break;
                case 7:
                    processArrayList();
                    break;

            }
        }

    }

    public static void printInstructions() {

        System.out.println("\nPress ");
        System.out.println("\t 0 - To print choice options ");
        System.out.println("\t 1 - To print the list of grocery items.");
        System.out.println("\t 2 - To add an item to the list. ");
        System.out.println("\t 3 - To modify an item in the list. ");
        System.out.println("\t 4 - To Remove an item in the list. ");
        System.out.println("\t 5 - To search for an item in the list. ");
        System.out.println("\t 6 - To quit the application ");
    }

    public static void addItem() {
        System.out.println("Please enter the grocery item: ");
        groceryList.addItem(scanner.nextLine());
    }

    public static void modifyItem() {
        System.out.println("Current Item name: ");
        String itemNo = scanner.nextLine();
        System.out.println("Enter replacement item");
        String newItem = scanner.nextLine();
        groceryList.modifyItem(itemNo, newItem); // deduct on to make a human readable
    }

    public static void removeItem() {
            System.out.println("Please enter item number: ");
            String itemNo = scanner.nextLine();
            scanner.nextLine();
            groceryList.removeItem(itemNo);
        }

        public static void searchItem(){
            System.out.println("Enter item to search for ");
            String searchItem = scanner.nextLine();
            if (groceryList.onFile(searchItem)){
                System.out.println("Found " + searchItem);
            }
            else {
                System.out.println(searchItem + " is not on file");
            }
        }

    public static void processArrayList(){
        ArrayList<String> newArray = new ArrayList<String>();
        newArray.addAll(groceryList.getGroceryList());
        //OR
        ArrayList<String> nextArray = new ArrayList<String>(groceryList.getGroceryList()); // this will happen at the point of time that you declare and initialise the new object for the new array list.
        //OR to convert the arraylist into an array
        String [] myArray = new String[groceryList.getGroceryList().size()];
        myArray = groceryList.getGroceryList().toArray(myArray);

    }
    }


/*
ArrayLists handler resizing automatically
*/
