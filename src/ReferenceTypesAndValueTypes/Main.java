package ReferenceTypesAndValueTypes;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int myIntValue = 10;
        int anotherIntValue = myIntValue;
        System.out.println("AnotherIntValue = " + anotherIntValue);

        anotherIntValue++;

        System.out.println("AnotherIntValue = " + anotherIntValue);

        int[] myIntArray = new int[5]; // reference that points to array in memory.
        int[] anotherIntArray = myIntArray; // two reference pointing to the same array in memory
        //both the arrays hold the same address, new equals new object.

        System.out.println("myIntArray " + Arrays.toString(myIntArray));
        System.out.println("anotherArray " + Arrays.toString(anotherIntArray));

        anotherIntArray[0] = 1; // assign value of one to the first element to value of 1, both references will see the same.

        System.out.println("after change myIntArray " + Arrays.toString(myIntArray));
        System.out.println("after change anotherArray " + Arrays.toString(anotherIntArray));

        anotherIntArray = new int[]{4,5,6,7,8}; // This will create a different array in memory.
        modifyArray(myIntArray); //this should change the first element, since this is also referencing the same array.

        System.out.println("after modify myIntArray " + Arrays.toString(myIntArray));
        System.out.println("after modify anotherArray " + Arrays.toString(anotherIntArray));
    }

    private static void modifyArray(int[] array){ // a method cant modify a reference itself only dereference
        array[0] = 2;

        array = new int[]{ 1,2,3,4,5}; // creates a new array and initialized with content.
    }
}



/*when we created a an int variable this value top a single space in memory is allocated to
store the value and that variable directly holds a value now if you'll sign it to another variable the value is
copied directly and then both variables work independently as you can see and of
each have their own copy of a specific value in the case of might value it's 10 and another in value 11

Reference types hold a reference or an address to the object but not the actual object for example an array element.
*/