package ReferenceTypesAndValueTypes;

import java.util.Arrays;

public class ReverseArrayChallange {
    public static void main(String[] args) {

        int [] array = {1,5,3,7,11,9,15};
        System.out.println("Array = " + Arrays.toString(array));

        reverse(array);

        System.out.println("Reverse Array = " + Arrays.toString(array));
    }

    private static void reverse(int[] array) {
        int maxIndex = array.length - 1; //represent maximum index

        int halfLength = array.length / 2; // loops to half way of the array.

        for (int i = 0; i < halfLength; i++) {
            int temp = array[i]; // Store first element in temp variable
            array[i] = array[maxIndex - i]; //sets element at index 0 to value of the last element
            array[maxIndex - i] = temp; //changes the last index to a value of 1 since temp has a value of 1
        }


    }
}
